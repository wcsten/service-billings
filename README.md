# Olist Challenge - Service Billings API
This is an application proposed by Olist's tech team at https://github.com/olist/work-at-olist.


### API Documentation
To get monthly bills through the REST API you can access the documentation at https://billings-api-test.herokuapp.com/docs/

### Running locally
#### Requirements
- A virtualenv with Python 3.7+ installed.
- SQLite3 installed on your machine.

### Cloning

Clone this repo by running  `git clone git@bitbucket.org:wcsten/service-billings.git` on your terminal.

Run `pip install pipenv`

Then cd to `/service-billings`.

Run `pipenv install --dev`

And finaly to run virtualenv run `pipenv shell`

### Creating your local settings file

Create a `.env` file with the following content:

```
SECRET_KEY=mysecret
DEBUG=True
ALLOWED_HOSTS='*'
DATABASE_URL=sqlite:///db.sqlite3
```

### Installing the application
The command bellow will install the required dependencies for running the project on your virtualenv.

Run `make install`

Run `make loaddata` to create a initial data to application work.

Running the applicaton server
Run `make start`. Access the application on your browser at `http://0.0.0.0:5000/docs/`

If you have not yet configured the `service-calls` application, access this link to finish the setup to run the application locally [service-calls documentation](https://bitbucket.org/wcsten/service-calls/src/master/).

`http://0.0.0.0:5000/v1/billings`- Provides an endpoint to get billings details if you have call ended records in `service-calls` 

### Running tests
Run `make test` on your terminal.


### Work environment
- Visual Studio Code
- MacBook Pro (13-inch, 2017) / Mac OS High Sierra
- Pyenv
- Pip
- Flake8
- Django, Django Rest Framework, Python Decouple
