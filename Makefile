install:
	@cd src/ && python manage.py migrate --settings=settings.settings
	@cd src/ && python manage.py runserver 0.0.0.0:5000 --settings=settings.settings

loaddata:
	@cd src/ && python manage.py loaddata price_rules.json --settings=settings.settings

shell:
	@cd src/ && python manage.py shell --settings=settings.settings

test:
	@cd src/ && python manage.py test billings --settings=settings.settings

start:
	@cd src/ && python manage.py runserver 0.0.0.0:5000 --settings=settings.settings
