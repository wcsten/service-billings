from django.urls import include, path
from rest_framework import routers

from django.views.generic import RedirectView

from rest_framework_swagger.views import get_swagger_view


swagger_schema_view = get_swagger_view(title='Billings API')

router = routers.DefaultRouter()

urlpatterns = [
    path('v1/', include('billings.urls')),
    path('', RedirectView.as_view(permanent=True, url='/docs/')),
    path(r'docs/', swagger_schema_view),
]
