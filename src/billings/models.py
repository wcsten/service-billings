from django.db import models

from .calculate_tariff import CalculateFactory

from billings.managers import CreateChargeManager


class Bill(models.Model):
    subscriber = models.CharField(
        'Subscriber number', max_length=11, blank=True, null=True, db_index=True
    )
    year = models.IntegerField('Reference year', db_index=True)
    month = models.IntegerField('Reference month', db_index=True)
    amount = models.DecimalField('Amount', decimal_places=2, max_digits=10, default=0)

    class Meta:
        verbose_name = 'Bill'
        verbose_name_plural = 'Bills'

    def __str__(self):
        return f'{self.month}/{self.year} bill for the subscriber {self.subscriber}'


class Charge(models.Model):
    bill = models.ForeignKey(Bill, on_delete=models.CASCADE, related_name='calls')
    call_id = models.PositiveIntegerField('Call_ID', db_index=True)
    destination = models.CharField('Destination number', max_length=11)
    duration = models.DurationField('Duration')
    start_at = models.DateTimeField('start at')
    price = models.DecimalField('Price', decimal_places=2, max_digits=10)

    objects = CreateChargeManager()

    class Meta:
        verbose_name = 'Charge'
        verbose_name_plural = 'Charges'

    def __str__(self):
        return f'Charge of R$ {self.price:.2f} for the call {self.call_id}'


class PriceRule(models.Model):
    standing_price = models.DecimalField('Standing price', decimal_places=2,
                                         max_digits=10)
    reduced_tariff_price = models.DecimalField('Reduced tariff price', decimal_places=2,
                                               max_digits=10)
    standard_tariff_price = models.DecimalField('Standard tariff price', decimal_places=2,
                                                max_digits=10)
    reduced_tariff_start_time = models.TimeField('Free tariff start time')
    reduced_tariff_end_time = models.TimeField('Free tariff end time')
    date_added = models.DateTimeField('Date added', auto_now_add=True)

    class Meta:
        verbose_name = 'Price Rule'
        verbose_name_plural = 'Price Rules'

    def get_call_price(self, started_at, ended_at, duration):
        instance = CalculateFactory.get_instance(
            started_at.time(),
            ended_at.time(),
            duration,
            self,
        )
        return instance.calculate(started_at, ended_at)
