import math
from datetime import datetime


class BaseCalculate:
    MINUTE = 60

    def __init__(self, price_rule, duration_delta):
        self.price_rule = price_rule
        self.duration_delta = duration_delta

    def get_duration_in_minutes(self):
        duration = self.duration_delta.total_seconds() // BaseCalculate.MINUTE
        return math.floor(duration)

    def calculate(self, started_at, ended_at):
        reduced_tariff_price = self.price_rule.reduced_tariff_price
        standard_tariff_price = self.price_rule.standard_tariff_price

        end_standard_total_minutes = self.get_end_standard_tariff_time(ended_at)
        end_standard_price = standard_tariff_price * end_standard_total_minutes

        end_reduced_total_minutes = self.get_end_reduced_tariff_time(ended_at)
        end_reduced_price = reduced_tariff_price * end_reduced_total_minutes

        start_reduced_total_minutes = self.get_start_reduced_tariff_time(started_at)
        started_reduced_price = reduced_tariff_price * start_reduced_total_minutes

        start_standard_total_minutes = self.get_start_standard_tariff_time(started_at)
        started_standard_price = standard_tariff_price * start_standard_total_minutes

        reduced_price = end_reduced_price + started_reduced_price
        standard_price = end_standard_price + started_standard_price

        return (reduced_price + standard_price)

    def get_start_reduced_tariff_time(self, started_at):
        return 0

    def get_start_standard_tariff_time(self, started_at):
        return 0

    def get_end_reduced_tariff_time(self, ended_at):
        return 0

    def get_end_standard_tariff_time(self, ended_at):
        return 0


class CalculateStartsAndEndReduced(BaseCalculate):

    def calculate(self, started_at, ended_at):
        subtotal = super().calculate(started_at, ended_at)
        total = self.price_rule.standing_price + (
            self.get_duration_in_minutes() * self.price_rule.reduced_tariff_price)
        return total + subtotal


class CalculateStartsAndEndStandard(BaseCalculate):

    def calculate(self, started_at, ended_at):
        subtotal = super().calculate(started_at, ended_at)
        total = self.price_rule.standing_price + (
            self.get_duration_in_minutes() * self.price_rule.standard_tariff_price)
        return total + subtotal


class CalculateStartStandardEndReduced(BaseCalculate):

    def get_start_standard_tariff_time(self, started_at):
        reduced_tariff_start_time = self.price_rule.reduced_tariff_start_time
        standard_tariff_time = (datetime.combine(
            started_at.date(), reduced_tariff_start_time) - started_at).total_seconds()
        standard_tariff_time = standard_tariff_time // BaseCalculate.MINUTE
        standard_total_minutes = math.floor(standard_tariff_time)
        return standard_total_minutes

    def get_end_reduced_tariff_time(self, ended_at):
        reduced_tariff_start_time = self.price_rule.reduced_tariff_start_time
        reduced_tariff_time = (ended_at - datetime.combine(
            ended_at.date(), reduced_tariff_start_time)).total_seconds()
        reduced_tariff_time = reduced_tariff_time // BaseCalculate.MINUTE
        reduced_total_minutes = math.floor(reduced_tariff_time)
        return reduced_total_minutes

    def calculate(self, started_at, ended_at):
        subtotal = super().calculate(started_at, ended_at)
        total = self.price_rule.standing_price + subtotal
        return total


class CalculateStartReducedEndStandard(BaseCalculate):

    def get_start_reduced_tariff_time(self, started_at):
        reduced_tariff_end_time = self.price_rule.reduced_tariff_end_time
        reduced_tariff_time = (datetime.combine(
            started_at.date(), reduced_tariff_end_time) - started_at).total_seconds()
        reduced_tariff_time = reduced_tariff_time // BaseCalculate.MINUTE
        reduced_total_minutes = math.floor(reduced_tariff_time)
        return reduced_total_minutes

    def get_end_standard_tariff_time(self, ended_at):
        reduced_tariff_end_time = self.price_rule.reduced_tariff_end_time
        standard_tariff_time = (ended_at - datetime.combine(
            ended_at.date(), reduced_tariff_end_time)).total_seconds()
        standard_tariff_time = standard_tariff_time // BaseCalculate.MINUTE
        standard_total_minutes = math.floor(standard_tariff_time)
        return standard_total_minutes

    def calculate(self, started_at, ended_at):
        subtotal = super().calculate(started_at, ended_at)
        total = self.price_rule.standing_price + subtotal
        return total


class CalculateFactory:

    @classmethod
    def get_instance(cls, started_at, ended_at, duration, price_rule):
        started_in_range = cls.in_range(price_rule, started_at)
        ended_in_range = cls.in_range(price_rule, ended_at)

        if started_in_range and ended_in_range:
            return CalculateStartsAndEndReduced(price_rule, duration)

        if not started_in_range and not ended_in_range:
            return CalculateStartsAndEndStandard(price_rule, duration)

        if started_in_range and not ended_in_range:
            return CalculateStartReducedEndStandard(price_rule, duration)

        if not started_in_range and ended_in_range:
            return CalculateStartStandardEndReduced(price_rule, duration)

    @classmethod
    def in_range(cls, price_rule, time_at):
        start = price_rule.reduced_tariff_start_time
        end = price_rule.reduced_tariff_end_time
        if start <= end:
            return start <= time_at <= end
        return start <= time_at or time_at <= end
