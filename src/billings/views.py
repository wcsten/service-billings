import coreapi

from django.http import Http404

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.schemas import AutoSchema

from billings.serializers import (
    CreateChargeSerializer, RetrieveBillSerializer, ValidateBillParamsSerializer
)

from billings.models import Bill


class ChargeAPIView(APIView):

    def post(self, request, format=None):
        serializer = CreateChargeSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_201_CREATED)


class RetrieveBillAPIView(APIView):
    schema = AutoSchema(
        manual_fields=[
            coreapi.Field(
                'month',
                required=False,
                location='query',
                description='A Month to be looked up for. Example: 12'
            ),
            coreapi.Field(
                'year',
                required=False,
                location='query',
                description='A Year to be looked up for. Example: 2017'
            ),
            coreapi.Field(
                'subscriber',
                required=False,
                location='query',
                description='A Subscriber to be looked up for. Example: 16981311111'
            ),
        ]
    )

    def get(self, request, format=None):
        serializer = ValidateBillParamsSerializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)

        try:
            bill = Bill.objects.prefetch_related('calls').get(
                subscriber=serializer.validated_data['subscriber'],
                month=serializer.validated_data['month'],
                year=serializer.validated_data['year'])
        except Bill.DoesNotExist as err:
            raise Http404(str(err))

        representation_serializer = RetrieveBillSerializer(bill)
        return Response(data=representation_serializer.data)
