from django.db import models, transaction

from billings import models as billing_models

import logging

logger = logging.getLogger('billings')


class CreateChargeManager(models.Manager):

    def custom_create(self, charge_data):
        call_id = charge_data['call_id']
        prefix = '[Create charge] -'
        logger.info(f'{prefix} Start to create charge for call_id: {call_id}')
        try:
            price_rule = billing_models.PriceRule.objects.latest('date_added')
        except billing_models.PriceRule.DoesNotExist as err:
            logger.error(f'{prefix} Error PriceRule DoesNotExist message: {err}')
            raise err

        call_price = price_rule.get_call_price(
            charge_data['start_at'], charge_data['end_at'], charge_data['duration'])

        with transaction.atomic():
            current_bill, _ = billing_models.Bill.objects.get_or_create(
                subscriber=charge_data['subscriber'],
                month=charge_data['start_at'].month,
                year=charge_data['start_at'].year,
            )
            charge = billing_models.Charge.objects.create(
                bill=current_bill,
                call_id=charge_data['call_id'],
                destination=charge_data['destination'],
                duration=charge_data['duration'],
                start_at=charge_data['start_at'],
                price=call_price,
            )
            current_bill.amount += call_price
            current_bill.save()
        logger.info(
            f'{prefix} Charge create successful charge_id: {charge.id} call_id: {call_id}'
        )
        return charge
