from rest_framework import serializers

from billings.utils import is_phone_number_valid

from billings.models import Bill, Charge


class CreateChargeSerializer(serializers.Serializer):
    subscriber = serializers.CharField()
    destination = serializers.CharField()
    duration = serializers.DurationField()
    call_id = serializers.IntegerField()
    start_at = serializers.DateTimeField()
    end_at = serializers.DateTimeField()

    def validate_subscriber(self, value):
        if not is_phone_number_valid(value):
            raise serializers.ValidationError('Invalid phone number.')
        return value

    def validate_destination(self, value):
        if not is_phone_number_valid(value):
            raise serializers.ValidationError('Invalid phone number.')
        return value

    def validate_call_id(self, value):
        if Charge.objects.filter(call_id=value).exists():
            raise serializers.ValidationError(
                f'Charge with call_id: {value} already exists.')
        return value

    def validate(self, data):
        validated_data = super().validate(data)
        if validated_data['start_at'] >= validated_data['end_at']:
            raise serializers.ValidationError(
                "The start_at can not be greater than or equal to the start_at"
            )
        return validated_data

    def create(self, validated_data):
        return Charge.objects.custom_create(validated_data)


class CallRepresentationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Charge
        fields = ('call_id', 'start_at', 'duration', 'price')

    def to_representation(self, charge):
        return {
            'call_id': charge.call_id,
            'destination': charge.destination,
            'start_date': charge.start_at.date(),
            'start_time': charge.start_at.time(),
            'duration': charge.duration,
            'price': charge.price,
        }


class RetrieveBillSerializer(serializers.ModelSerializer):
    calls = CallRepresentationSerializer(many=True, read_only=True)

    class Meta:
        model = Bill
        fields = ('subscriber', 'month', 'year', 'calls')


class ValidateBillParamsSerializer(serializers.Serializer):
    subscriber = serializers.CharField()
    year = serializers.IntegerField()
    month = serializers.IntegerField()

    def validate_subscriber(self, value):
        if not is_phone_number_valid(value):
            raise serializers.ValidationError('Invalid phone number.')
        return value
