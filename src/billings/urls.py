from django.urls import path

from billings import views


urlpatterns = [
    path(r'charges/', views.ChargeAPIView.as_view(), name='charges'),
    path(r'billings/', views.RetrieveBillAPIView.as_view(), name='billings')
]
