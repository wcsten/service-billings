from datetime import datetime, timedelta

from rest_framework import status
from rest_framework.test import APITransactionTestCase
from rest_framework.reverse import reverse

from billings.tests.factories import BillFactory, ChargeFactory, PriceRuleFactory


class ChargeAPIViewTestCase(APITransactionTestCase):

    def setUp(self):
        self.price_rule = PriceRuleFactory.create()
        self.duration = timedelta(minutes=5)
        self.start_at = datetime(2019, 5, 12, 22, 0, 0)
        self.end_at = datetime(2019, 5, 12, 22, 5, 0)
        self.subscriber = '16999666668'
        self.destination = '16999666667'
        self.call_id = 1

    def get_call_payload(self):
        return {
            'subscriber': self.subscriber,
            'destination': self.destination,
            'call_id': self.call_id,
            'duration': self.duration,
            'start_at': self.start_at,
            'end_at': self.end_at
        }

    def test_create_charge_sucess(self):
        call_payload = self.get_call_payload()
        url = reverse('charges')
        response = self.client.post(url, data=call_payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_charge_without_call_id(self):
        call_payload = self.get_call_payload()
        call_payload.pop('call_id')
        url = reverse('charges')
        response = self.client.post(url, data=call_payload, format='json')
        errors = response.json()['call_id']
        self.assertEqual(errors[0], 'This field is required.')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_charge_with_start_at_greater_than_end_at(self):
        call_payload = self.get_call_payload()
        starts_greater = datetime(2019, 5, 12, 22, 5, 0)
        end_at = datetime(2019, 5, 12, 22, 0, 0)
        call_payload['start_at'] = starts_greater
        call_payload['end_at'] = end_at

        url = reverse('charges')
        response = self.client.post(url, data=call_payload, format='json')
        errors = response.json()['non_field_errors']
        self.assertEqual(
            errors[0], 'The start_at can not be greater than or equal to the start_at')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_charge_with_wrong_subscriber_number(self):
        call_payload = self.get_call_payload()
        call_payload['subscriber'] = '169996666611a'

        url = reverse('charges')
        response = self.client.post(url, data=call_payload, format='json')
        errors = response.json()['subscriber']
        self.assertEqual(errors[0], 'Invalid phone number.')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_charge_with_wrong_destination_number(self):
        call_payload = self.get_call_payload()
        call_payload['destination'] = '16999666667aa'

        url = reverse('charges')
        response = self.client.post(url, data=call_payload, format='json')
        errors = response.json()['destination']
        self.assertEqual(errors[0], 'Invalid phone number.')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_not_allowed(self):
        url = reverse('billings')
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class RetrieveBillAPIViewTestCase(APITransactionTestCase):

    def setUp(self):
        self.bill = BillFactory.create(amount=2.00)
        ChargeFactory.create(
            bill=self.bill,
            duration=timedelta(minutes=2),
            call_id=1,
            start_at=datetime(2019, 5, 12, 13, 0, 0)
        )
        ChargeFactory.create(
            bill=self.bill,
            duration=timedelta(minutes=2),
            call_id=2,
            start_at=datetime(2019, 5, 11, 8, 0, 0)
        )

    def test_get_bill_with_calls(self):
        url = reverse('billings')
        params = {
            'subscriber': self.bill.subscriber,
            'year': self.bill.year,
            'month': self.bill.month,
        }
        response = self.client.get(url, data=params, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_bill_not_found(self):
        url = reverse('billings')
        params = {
            'subscriber': '16981317314',
            'year': 2019,
            'month': 5,
        }
        response = self.client.get(url, data=params, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_post_not_allowed(self):
        url = reverse('billings')
        response = self.client.post(url, data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_delete_not_allowed(self):
        url = reverse('billings')
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_put_not_allowed(self):
        url = reverse('billings')
        response = self.client.put(url, data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_patch_not_allowerd(self):
        url = reverse('billings')
        response = self.client.patch(url, data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_get_with_no_params(self):
        url = reverse('billings')
        response = self.client.get(url, data={}, format='json')
        errors = response.json()
        self.assertEqual(errors['subscriber'][0], 'This field is required.')
        self.assertEqual(errors['year'][0], 'This field is required.')
        self.assertEqual(errors['month'][0], 'This field is required.')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_with_wrong_params_type(self):
        url = reverse('billings')
        params = {
            'subscriber': 11,
            'year': 'invalid data',
            'month': 'invalid data',
        }
        response = self.client.get(url, data=params, format='json')
        errors = response.json()
        self.assertEqual(errors['subscriber'][0], 'Invalid phone number.')
        self.assertEqual(errors['year'][0], 'A valid integer is required.')
        self.assertEqual(errors['month'][0], 'A valid integer is required.')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
