from datetime import datetime, time, timedelta
from django.test import TestCase

from billings.tests.factories import PriceRuleFactory
from billings.calculate_tariff import (
    CalculateFactory, CalculateStartReducedEndStandard,
    CalculateStartsAndEndReduced, CalculateStartsAndEndStandard,
    CalculateStartStandardEndReduced
)


class CalculateFactoryTest(TestCase):

    def setUp(self):
        self.price_rule = PriceRuleFactory.create()

    def test_return_calculate_starts_and_ends_reduced(self):
        started_at = time(22, 10, 0)
        ended_at = time(22, 20, 0)
        duration = timedelta(minutes=10)

        instance = CalculateFactory.get_instance(
            started_at,
            ended_at,
            duration,
            self.price_rule,
        )
        self.assertIsInstance(instance, CalculateStartsAndEndReduced)

    def test_return_calculate_starts_and_ends_standard(self):
        started_at = time(21, 10, 0)
        ended_at = time(21, 20, 0)
        duration = timedelta(minutes=10)

        instance = CalculateFactory.get_instance(
            started_at,
            ended_at,
            duration,
            self.price_rule,
        )
        self.assertIsInstance(instance, CalculateStartsAndEndStandard)

    def test_return_calculate_start_standard_end_reduced(self):
        started_at = time(21, 10, 0)
        ended_at = time(22, 20, 0)
        duration = timedelta(minutes=10)

        instance = CalculateFactory.get_instance(
            started_at,
            ended_at,
            duration,
            self.price_rule,
        )
        self.assertIsInstance(instance, CalculateStartStandardEndReduced)

    def test_return_calculate_start_reduced_end_standard(self):
        started_at = time(5, 10, 0)
        ended_at = time(7, 20, 0)
        duration = timedelta(minutes=10)

        instance = CalculateFactory.get_instance(
            started_at,
            ended_at,
            duration,
            self.price_rule,
        )
        self.assertIsInstance(instance, CalculateStartReducedEndStandard)


class CalculateStartsAndEndReducedTest(TestCase):
    def setUp(self):
        self.started_at = datetime(2019, 2, 22, 22, 10, 0)
        self.ended_at = datetime(2019, 2, 22, 22, 20, 0)
        self.duration = timedelta(minutes=10)
        self.price_rule = PriceRuleFactory.create()
        self.start_end_reduced = CalculateStartsAndEndReduced(
            self.price_rule, self.duration)

    def test_success_return_zero_in_get_tariff_methods(self):
        start_reduced = self.start_end_reduced.get_start_reduced_tariff_time(
            self.started_at)
        end_reduced = self.start_end_reduced.get_end_reduced_tariff_time(
            self.ended_at)
        start_standard = self.start_end_reduced.get_start_standard_tariff_time(
            self.started_at)
        end_standard = self.start_end_reduced.get_end_standard_tariff_time(
            self.ended_at)
        self.assertEqual(start_reduced, 0)
        self.assertEqual(end_reduced, 0)
        self.assertEqual(start_standard, 0)
        self.assertEqual(end_standard, 0)

    def test_calculate_success_price(self):
        call_price = self.start_end_reduced.calculate(
            self.started_at, self.ended_at)
        self.assertEqual(call_price, 0.36)


class CalculateStartsAndEndStandardTest(TestCase):
    def setUp(self):
        self.started_at = datetime(2019, 2, 22, 21, 10, 0)
        self.ended_at = datetime(2019, 2, 22, 21, 20, 0)
        self.duration = timedelta(minutes=10)
        self.price_rule = PriceRuleFactory.create()
        self.start_end_standard = CalculateStartsAndEndStandard(
            self.price_rule, self.duration)

    def test_success_return_zero_in_get_tariff_methods(self):
        start_reduced = self.start_end_standard.get_start_reduced_tariff_time(
            self.started_at)
        end_reduced = self.start_end_standard.get_end_reduced_tariff_time(
            self.ended_at)
        start_standard = self.start_end_standard.get_start_standard_tariff_time(
            self.started_at)
        end_standard = self.start_end_standard.get_end_standard_tariff_time(
            self.ended_at)
        self.assertEqual(start_reduced, 0)
        self.assertEqual(end_reduced, 0)
        self.assertEqual(start_standard, 0)
        self.assertEqual(end_standard, 0)

    def test_calculate_success_price(self):
        call_price = self.start_end_standard.calculate(
            self.started_at, self.ended_at)
        self.assertEqual(round(call_price, 2), 1.26)


class CalculateStartStandardEndReducedTest(TestCase):
    def setUp(self):
        self.started_at = datetime(2019, 2, 22, 21, 55, 0)
        self.ended_at = datetime(2019, 2, 22, 22, 5, 0)
        self.duration = timedelta(minutes=10)
        self.price_rule = PriceRuleFactory.create()
        self.start_standard_end_reduced = CalculateStartStandardEndReduced(
            self.price_rule, self.duration)

    def test_success_return_minutes_in_start_standard_and_end_reduced(self):
        start_reduced = self.start_standard_end_reduced.get_start_reduced_tariff_time(
            self.started_at)
        end_reduced = self.start_standard_end_reduced.get_end_reduced_tariff_time(
            self.ended_at)
        start_standard = self.start_standard_end_reduced.get_start_standard_tariff_time(
            self.started_at)
        end_standard = self.start_standard_end_reduced.get_end_standard_tariff_time(
            self.ended_at)
        self.assertEqual(start_reduced, 0)
        self.assertEqual(end_reduced, 5)
        self.assertEqual(start_standard, 5)
        self.assertEqual(end_standard, 0)

    def test_calculate_success_price(self):
        call_price = self.start_standard_end_reduced.calculate(
            self.started_at, self.ended_at)
        self.assertEqual(round(call_price, 2), 0.81)


class CalculateStartReducedEndStandardTest(TestCase):
    def setUp(self):
        self.started_at = datetime(2019, 2, 22, 5, 55, 0)
        self.ended_at = datetime(2019, 2, 22, 6, 5, 0)
        self.duration = timedelta(minutes=10)
        self.price_rule = PriceRuleFactory.create()
        self.start_reduced_end_standard = CalculateStartReducedEndStandard(
            self.price_rule, self.duration)

    def test_success_return_minutes_in_start_reduced_and_end_standard(self):
        start_reduced = self.start_reduced_end_standard.get_start_reduced_tariff_time(
            self.started_at)
        end_reduced = self.start_reduced_end_standard.get_end_reduced_tariff_time(
            self.ended_at)
        start_standard = self.start_reduced_end_standard.get_start_standard_tariff_time(
            self.started_at)
        end_standard = self.start_reduced_end_standard.get_end_standard_tariff_time(
            self.ended_at)
        self.assertEqual(start_reduced, 5)
        self.assertEqual(end_reduced, 0)
        self.assertEqual(start_standard, 0)
        self.assertEqual(end_standard, 5)

    def test_calculate_success_price(self):
        call_price = self.start_reduced_end_standard.calculate(
            self.started_at, self.ended_at)
        self.assertEqual(round(call_price, 2), 0.81)
