from datetime import time
from factory import SubFactory
from factory.django import DjangoModelFactory
from billings.models import Bill, PriceRule, Charge


class BillFactory(DjangoModelFactory):
    class Meta:
        model = Bill

    subscriber = '16999996666'
    year = 2019
    month = 6
    amount = 1000.00


class ChargeFactory(DjangoModelFactory):
    class Meta:
        model = Charge

    bill = SubFactory(BillFactory)
    destination = '16999996777'
    price = 1.00


class PriceRuleFactory(DjangoModelFactory):
    class Meta:
        model = PriceRule

    standing_price = 0.36
    reduced_tariff_price = 0.00
    standard_tariff_price = 0.09
    reduced_tariff_start_time = time(22, 0, 0)
    reduced_tariff_end_time = time(6, 0, 0)
