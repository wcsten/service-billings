from datetime import datetime, time, timedelta

from django.test import TestCase

from billings.tests.factories import BillFactory, ChargeFactory, PriceRuleFactory
from billings.models import Bill, Charge, PriceRule


class BillModelTest(TestCase):

    def test_create(self):
        bill = BillFactory.create(
            subscriber='16999996666',
            year=2019,
            month=6,
            amount=1000.00,
        )
        self.assertIsInstance(bill, Bill)


class ChargeModelTest(TestCase):

    def test_create(self):
        charge = ChargeFactory.create(
            call_id=1,
            duration=timedelta(minutes=2),
            start_at=datetime(2019, 5, 12, 12, 0, 0),
        )
        self.assertIsInstance(charge, Charge)


class PriceRuleModelTest(TestCase):

    def setUp(self):
        self.price_rule = PriceRuleFactory.create()

    def get_call_payload(self):
        return {
            'subscriber': '16999666668',
            'destination': '16999666667',
            'call_id': 1,
            'start_at': datetime(2019, 5, 12, 22, 0, 0),
            'end_at': datetime(2019, 5, 12, 22, 5, 0)
        }

    def test_create(self):
        self.assertIsInstance(self.price_rule, PriceRule)

    def test_get_call_price(self):
        call_payload = self.get_call_payload()
        duration = call_payload['end_at'] - call_payload['start_at']
        call_price = self.price_rule.get_call_price(
            call_payload['start_at'], call_payload['end_at'], duration)
        self.assertEqual(call_price, 0.36)

    def test_get_call_price_with_standard_time(self):
        call_payload = self.get_call_payload()
        call_payload['start_at'] = datetime(2019, 5, 12, 12, 0, 0)
        call_payload['end_at'] = datetime(2019, 5, 12, 12, 5, 0)
        duration = call_payload['end_at'] - call_payload['start_at']

        call_price = self.price_rule.get_call_price(
            call_payload['start_at'], call_payload['end_at'], duration)
        self.assertEqual(round(call_price, 2), 0.81)

    def test_get_call_price_with_start_reduced_time(self):
        call_payload = self.get_call_payload()
        call_payload['start_at'] = datetime(2019, 5, 12, 5, 50, 0)
        call_payload['end_at'] = datetime(2019, 5, 12, 6, 10, 0)
        duration = call_payload['end_at'] - call_payload['start_at']

        call_price = self.price_rule.get_call_price(
            call_payload['start_at'], call_payload['end_at'], duration)
        self.assertEqual(round(call_price, 2), 1.26)

    def test_get_call_price_with_end_reduced_time(self):
        call_payload = self.get_call_payload()
        call_payload['start_at'] = datetime(2019, 5, 12, 21, 45, 0)
        call_payload['end_at'] = datetime(2019, 5, 12, 22, 10, 0)
        duration = call_payload['end_at'] - call_payload['start_at']

        call_price = self.price_rule.get_call_price(
            call_payload['start_at'], call_payload['end_at'], duration)
        self.assertEqual(round(call_price, 2), 1.71)

    def test_get_call_with_anoter_price_rule(self):
        price_rule = PriceRuleFactory.create(
            standing_price=0.40,
            reduced_tariff_price=0.05,
            standard_tariff_price=0.10,
            reduced_tariff_start_time=time(22, 0, 0),
            reduced_tariff_end_time=time(6, 0, 0)
        )

        call_payload = self.get_call_payload()
        call_payload['start_at'] = datetime(2019, 5, 12, 21, 00, 0)
        call_payload['end_at'] = datetime(2019, 5, 12, 22, 30, 0)
        duration = call_payload['end_at'] - call_payload['start_at']

        call_price = price_rule.get_call_price(
            call_payload['start_at'], call_payload['end_at'], duration)
        self.assertEqual(round(call_price, 2), 7.90)

    def test_get_call_with_no_costs(self):
        price_rule = PriceRuleFactory.create(
            standing_price=0.0,
            reduced_tariff_price=0.0,
            standard_tariff_price=0.0,
            reduced_tariff_start_time=time(22, 0, 0),
            reduced_tariff_end_time=time(6, 0, 0)
        )

        call_payload = self.get_call_payload()
        call_payload['start_at'] = datetime(2019, 5, 12, 21, 00, 0)
        call_payload['end_at'] = datetime(2019, 5, 12, 22, 30, 0)
        duration = call_payload['end_at'] - call_payload['start_at']

        call_price = price_rule.get_call_price(
            call_payload['start_at'], call_payload['end_at'], duration)
        self.assertEqual(round(call_price, 2), 0.0)

    def test_get_call_with_only_standing_price(self):
        price_rule = PriceRuleFactory.create(
            standing_price=1.0,
            reduced_tariff_price=0.0,
            standard_tariff_price=0.0,
            reduced_tariff_start_time=time(22, 0, 0),
            reduced_tariff_end_time=time(6, 0, 0)
        )

        call_payload = self.get_call_payload()
        call_payload['start_at'] = datetime(2019, 5, 12, 21, 00, 0)
        call_payload['end_at'] = datetime(2019, 5, 12, 22, 30, 0)
        duration = call_payload['end_at'] - call_payload['start_at']

        call_price = price_rule.get_call_price(
            call_payload['start_at'], call_payload['end_at'], duration)
        self.assertEqual(round(call_price, 2), 1.0)
